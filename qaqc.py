import piplates.DAQC2plate as DAQC2
import RPi.GPIO as GPIO
import time
import math
from smbus2 import SMBus, i2c_msg

def temperature():
    print("NTC\t\ttemp")
    R0  = 10000
    b   = 3936
    Rfx = 91000
    To  = 298  #To of NTC
    for i in range(0, 8):
        deltav = DAQC2.getADC(0, 8)
        Vin    = DAQC2.getADC(0, i)
        Ri     = R0 * math.exp(-b / To)
        Rn     = (Vin * Rfx)/(deltav - Vin)
        T      = b / (math.log(Rn / Ri)) - 273
        print(f'\t{i}: {round(T, 1)}\n')

def humidity():

    print("\nHYT\t\ttemp\tRH")
    for i in [11, 12, 13, 14, 15]:
        address = i
        hexAddress = int(str(address), 16) #0x28

        bus = SMBus(1)

        # Do a read
        msg = i2c_msg.write(hexAddress,[0])
        bus.i2c_rdwr(msg)
        time.sleep(0.2)

        # Do the read
        msg = i2c_msg.read(hexAddress, 4)
        bus.i2c_rdwr(msg)

        # Print the message
        data        = list(msg)
        humidity    = round(((data[0] & 0x3f) << 8 | data[1]) * (100.0 / 0x3fff), 1)
        temperature = round((data[2] << 8 | (data[3] & 0xfc)) * (165.0 / 0xfffc) - 40.0, 1)
        print(f'\t{i}:\t{temperature}\t{humidity}\n')

        bus.close()

def polaritySwitch():
    print("Polarity switch test, listen to the relays.")
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(21, GPIO.OUT, initial = GPIO.LOW) # A
    GPIO.setup(26, GPIO.OUT, initial = GPIO.LOW) # B
    GPIO.setup(20, GPIO.OUT, initial = GPIO.LOW) # C
    GPIO.setup(16, GPIO.OUT, initial = GPIO.LOW) # D

    GPIO.setup(21, GPIO.OUT, initial = GPIO.HIGH) # A
    time.sleep(0.25)
    GPIO.setup(26, GPIO.OUT, initial = GPIO.HIGH) # B
    time.sleep(0.25)
    GPIO.setup(20, GPIO.OUT, initial = GPIO.HIGH) # C
    time.sleep(0.25)
    GPIO.setup(16, GPIO.OUT, initial = GPIO.HIGH) # D
    time.sleep(1.0)

    GPIO.setup(21, GPIO.OUT, initial=GPIO.LOW) # A
    time.sleep(0.25)
    GPIO.setup(26, GPIO.OUT, initial=GPIO.LOW) # B
    time.sleep(0.25)
    GPIO.setup(20, GPIO.OUT, initial=GPIO.LOW) # C
    time.sleep(0.25)
    GPIO.setup(16, GPIO.OUT, initial=GPIO.LOW) # D
    
    GPIO.cleanup()

def voltageSupply():

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(13, GPIO.OUT, initial=GPIO.LOW)
    GPIO.setup(13, GPIO.OUT, initial=GPIO.HIGH)
    print("\n24V is turned on, check your multimeter!")
    time.sleep(5.0)
    GPIO.setup(13, GPIO.OUT, initial=GPIO.LOW)
    GPIO.cleanup()

def main():
    temperature()
    humidity()
    polaritySwitch()
    voltageSupply()



if __name__ == "__main__":
    main()
